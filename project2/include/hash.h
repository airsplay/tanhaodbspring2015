#include "db.h"
#include "page.h"
//#include "list.h"
//#include <list>
#include <iostream>
#include <cstdlib>

class hashMap {
	struct element {
		int pid, fid;
		element() {
			pid = 0;
			fid = 0;
		}
		element(int pid, int fid) {
			this->pid = pid;
			this->fid = fid;
		}
		bool operator==(const element &x) const {
			return (pid == x.pid) && (fid == x.fid);
		}
	};
	class list {
		class block {
			public:
				element t;
				block* next;
				block* prev;
				block() {

				}
				block(element a){
					t = a;
				}
		};
		block* head;
		block* tail;
		void link(block* a, block* b) {
			a->next = b;
			b->prev = a;
		}
		void add(block* a, block* b, block * c) {
			b->next = a->next;
			a->next = b;
			b->prev = c->prev;
			c->prev = b;
		}
		void remove(block* a) {
			a->next->prev = a->prev;
			a->prev->next = a->next;
		}
		public:
		class iterator {
			block * p;
			public:
			iterator(block* p) {
				this->p = p;
			}
			iterator operator++ () {
				p = p->next;
				return *this;
			}
			iterator(const iterator & i) {
				p = i.p;
			}
			element* operator->() {
				return &(p->t);
			}
			bool operator==(const iterator& i) const {
				return (p == i.p);
			}
			bool operator!=(const iterator& i) const {
				return (p != i.p);
			}

			element operator* () const {
				return p->t;
			}
		};
		list() {
			head = new block;
			tail = new block;
			//printf("%d, %d", head, tail);
			link(head, tail);
			//printf("shit");
		}
		void push_back(element t) {
			add(tail->prev, new block(t), tail);
		}
		iterator begin() {
			return iterator(head->next);
		}
		iterator end() {
			return iterator(tail);
		}
		void remove(element t) {
			for (block* p = head->next; p != tail; p = p->next) {
				if (p -> t == t) {
					remove(p);
					return;
				}
			}
			return;
		}
	};
	static const long long a = 2147483647;
	static const long long b = 1232123212;

	int hash(int x) const {
		return (a * (long long)x + b) % num;
	}
	list *hashTable;
	int num;
	public :
	bool hasKey (PageId pid) {
		int hashCode = hash(pid);
		//cout << "haskey hash"<< hash(pid) << endl;
		for (list::iterator iter = hashTable[hashCode].begin(); iter != hashTable[hashCode].end(); ++ iter) {
			if (iter -> pid == pid) {
				return true;
			}
		}
		return false;
	}

	int operator[] (PageId pid) const {
		int hashCode = hash(pid);
		//cout << "[] hash"<< hashCode << endl;
		//cout << hashTable[hashCode].size() << endl;
		//cout << "shit" << pid  << endl;
		int ans = -1;
		for (list::iterator iter = hashTable[hashCode].begin(); iter != hashTable[hashCode].end(); ++ iter) {
			//cout << iter -> pid << endl;
			if (iter -> pid == pid) {
				ans = iter-> fid;
			}
		}
		return ans;
	}
	void link (PageId pid, int fid) {
		//cout << "link hash"<< hash(pid) << endl;
		hashTable[hash(pid)].push_back(element(pid, fid));
	}
	void unlink (PageId pid) {
		//cout << "shit unlink!!!" << endl;
		int fid = (*this)[pid];
		hashTable[hash(pid)].remove(element(pid, fid));
	}
	~hashMap() {
		delete []hashTable;
	}
	public:
	hashMap(int num) {
		//std::cout << "shit";
		//printf("fuckfuck");
		hashTable = new list[num];	
		this->num = num;
	}
};

