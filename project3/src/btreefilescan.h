/*
 * btreefilescan.h
 *
 * sample header file
 *
 */
 
#ifndef _BTREEFILESCAN_H
#define _BTREEFILESCAN_H

#include "btfile.h"

// errors from this class should be defined in btfile.h

class BTreeFileScan : public IndexFileScan {
	RID nowRid;
	PageId nowPid;
	void *lo_key;
	void *hi_key;
	void *key;
	AttrType key_type;
	int key_size;
	RID dataRid;
	bool pointed;
public:
    friend class BTreeFile;
	BTreeFileScan (const PageId pid, const RID rid, void * lo_key, void * hi_key, AttrType key_type, int key_size);

    // get the next record
    Status get_next(RID & rid, void* keyptr);

    // delete the record currently scanned
    Status delete_current();

    int keysize(); // size of the key

    // destructor
    ~BTreeFileScan();
private:

};

#endif
